/**
 * High DPI (4K and above) Swing Support: scaling-relevant resources.
 * 
 * @since 2.6.4
 * @author Dimitar Garkov (dim8)
 * @version 0.8
 */
package org.vanted.scaling.resources;